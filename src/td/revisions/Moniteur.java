package td.revisions;

public class Moniteur extends ChargeEnseignement {
    private final Etudiant etudiant;

    public Moniteur(Etudiant etudiant, int heuresService) {
        super(etudiant.nom, etudiant.prenom, heuresService);
        this.etudiant = etudiant;
        MINIMUM_HEURES = 64;
    }

    @Override
    public String toString() {
        return etudiant.toString() + ", moniteur, service prévu " + heuresService + "h";
    }
}
