package td.revisions;

public class Professeur extends ChargeEnseignement {
    final String classe;

    public Professeur(String nom, String prenom, String classe, int nombreHeures) {
        super(nom, prenom, nombreHeures);
        this.classe = classe;
        MINIMUM_HEURES = 192;
    }

    @Override
    public String toString() {
        return nom + ' ' + prenom + ", labo " + classe + ", service prévu " + heuresService + 'h';
    }
}
